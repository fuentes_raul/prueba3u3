
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ANDRES
 */
@Entity
@Table(name = "vehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculos.findAll", query = "SELECT v FROM Vehiculos v"),
    @NamedQuery(name = "Vehiculos.findByVehId", query = "SELECT v FROM Vehiculos v WHERE v.vehId = :vehId"),
    @NamedQuery(name = "Vehiculos.findByVehMarca", query = "SELECT v FROM Vehiculos v WHERE v.vehMarca = :vehMarca"),
    @NamedQuery(name = "Vehiculos.findByVehModelo", query = "SELECT v FROM Vehiculos v WHERE v.vehModelo = :vehModelo"),
    @NamedQuery(name = "Vehiculos.findByVehColor", query = "SELECT v FROM Vehiculos v WHERE v.vehColor = :vehColor"),
    @NamedQuery(name = "Vehiculos.findByVehAno", query = "SELECT v FROM Vehiculos v WHERE v.vehAno = :vehAno")})
public class Vehiculos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "veh_id")
    private String vehId;
    @Size(max = 2147483647)
    @Column(name = "veh_marca")
    private String vehMarca;
    @Size(max = 2147483647)
    @Column(name = "veh_modelo")
    private String vehModelo;
    @Size(max = 2147483647)
    @Column(name = "veh_color")
    private String vehColor;
    @Size(max = 2147483647)
    @Column(name = "veh_ano")
    private String vehAno;

    public Vehiculos() {
    }

    public Vehiculos(String vehId) {
        this.vehId = vehId;
    }

    public String getVehId() {
        return vehId;
    }

    public void setVehId(String vehId) {
        this.vehId = vehId;
    }

    public String getVehMarca() {
        return vehMarca;
    }

    public void setVehMarca(String vehMarca) {
        this.vehMarca = vehMarca;
    }

    public String getVehModelo() {
        return vehModelo;
    }

    public void setVehModelo(String vehModelo) {
        this.vehModelo = vehModelo;
    }

    public String getVehColor() {
        return vehColor;
    }

    public void setVehColor(String vehColor) {
        this.vehColor = vehColor;
    }

    public String getVehAno() {
        return vehAno;
    }

    public void setVehAno(String vehAno) {
        this.vehAno = vehAno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehId != null ? vehId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculos)) {
            return false;
        }
        Vehiculos other = (Vehiculos) object;
        if ((this.vehId == null && other.vehId != null) || (this.vehId != null && !this.vehId.equals(other.vehId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Vehiculos[ vehId=" + vehId + " ]";
    }
    
}

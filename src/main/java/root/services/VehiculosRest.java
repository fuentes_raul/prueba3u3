package root.services;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.model.dao.VehiculosDAO;
import root.persistence.entities.Vehiculos;

@Path("vehiculos")

public class VehiculosRest {

    VehiculosDAO dao = new VehiculosDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        List<Vehiculos> lista = dao.findVehiculosEntities();
        System.out.println("obteniendo lista de vehiculos desde DAO");
        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        Vehiculos vehiculo = dao.findVehiculos(idbuscar);
        System.out.println("Vehiculo encontrado por su ID");
        return Response.ok(200).entity(vehiculo).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(Vehiculos newVehiculo) throws Exception {
        VehiculosDAO vehiculoNuevo = new VehiculosDAO();
        vehiculoNuevo.create(newVehiculo);
        return Response.ok("Vehiculo ingresado correctamente").build();
    }

    @PUT
    public Response actualizar(Vehiculos actualizar) throws Exception {
        String resultado = null;
        VehiculosDAO vehiculo = new VehiculosDAO();
        vehiculo.edit(actualizar);
        return Response.ok("Datos modificados sin observaciones").build();
    }

    @DELETE
    @Path("/{iddelete}")
    public Response eliminarId(@PathParam("iddelete") String iddelete) throws Exception {
        VehiculosDAO vehiculo = new VehiculosDAO();
        vehiculo.destroy(iddelete);
        return Response.ok("Vehiculo eliminado con exito").build();
    }

}


<%-- 
    Document   : index
    Created on : 07-05-2020, 16:38:08
    Author     : ANDRES
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Alumno: Raúl A. Fuentes Palma</h3>
        <h3>Sección: 50 </h3>
        <h3>Asignatura: Taller de Aplicaciones Empresariales</h3>
        Método GET : Muestra el listado total de los vehículos alojados en la BD de PostgreSQL Heroku.<br>
        URL        : <a href="https://appvehiculosdb.herokuapp.com/api/vehiculos" >api/vehiculos</a><br><br>
        Método GET : Busca un vehículo específico por su ID alojado en la BD de PostgreSQL Heroku.<br>
        URL        : <a href="https://appvehiculosdb.herokuapp.com/api/vehiculos/02" >api/vehiculos/</a><br><br>
        Método POST : Agrega un vehículo a la BD de PostgreSQL Heroku.<br>
        URL        : <a href="https://appvehiculosdb.herokuapp.com/api/vehiculos/12" >api/vehiculos</a><br><br>
        Método PUT : Actualiza un vehículo en la BD de PostgreSQL Heroku.<br>
        URL        : <a href="https://appvehiculosdb.herokuapp.com/api/vehiculos/12" >api/vehiculos</a><br><br>
        Método DELETE : Elimina un vehículo en la BD de PostgreSQL Heroku.<br>
        URL        : <a href="https://appvehiculosdb.herokuapp.com/api/vehiculos/05" >api/vehiculos</a>(Se eliminó el vehículo)<br><br>

    </body>
</html>
